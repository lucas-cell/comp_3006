# comp_3006

### Python program to count the frequencies of characters in one or more text files.

The program uses argparse to handle command line arguments, and generates a dictionary containing each character and its frequency count from the input file(s). The final output shows the elements of the dictionary in CSV format. 


The program also includes three optional arguments (-c, -l, and -z) that can be used together or separately to modify the behavior of the program. The program's main function iterates through each file in the provided files list and calls the add_frequencies() function, passing in the 'd' dictionary, the current file, and the boolean value for 'remove_case'. 


The program's second function adds the frequencies of characters to the given dictionary by counting the occurrence of each character in the string data and stores it in the dictionary d. 


The program also handles three optional arguments: '-c' to distinguish between upper and lower case, '-l' to only print the given letters, and '-z' to print zeros for any missing letters. 

### Usage
python filename.py -h                                         # help command
python filename.py -c file1.txt                               # to distinguish between uppercase and lowercase
python filename.py -l az file1.txt file2.txt                  # to print given letters and count occurences from specific files
python filename.py -z file1.txt file2.txt        # to print zeros for missing letters

