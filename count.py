#!/usr/bin/env python3

# Student Name: Lucas Nieddu
# Student ID: 873664906
# Program Description: A program that counts the frequencies of characters in one or more text files.

import argparse
import csv
import sys


def add_frequencies(d, file, remove_case):
    """
    A function called add_frequencies that takes three parameters: a dictionary to be updated 
    with the frequency count of characters, a file path to read from, and a boolean flag indicating 
    if case sensitivity should be removed. The function reads the file and removes spaces, then, if 
    remove_case is True, it converts the data to lowercase. After that, it iterates through each character 
    in the data and adds the char as a new key with a frequency count of 1 if it doesn't already exist in 
    the dictionary, otherwise it increments the existing key's frequency count. The function then returns 
    the updated dictionary.
    """
    # Open the file and read the file into data and remove spaces from data
    with open(file, 'r') as f:
        data = f.read().replace(" ", "")

    # If remove_case is True, then transform data into lower case
    if remove_case:
        data = data.lower()

    # Count the occurrence of each character in the string data and store it in dictionary d.
    for char in data:
        if char in d:
            d[char] += 1
        else:
            d[char] = 1


def main():
    """
    Handles parsing the arguments, and calling add_frequencies(...). It should perform the following steps:

        1) Parse the command line arguments -c, -l, -z

        2) Create an empty dictionary

        3) Add the frequencies for each file in the argument list to that dictionary

        4) Print out the elements of that dictionary in CSV format
    """

    # 1) Parse the command line arguments -c, -l, -z
    parser = argparse.ArgumentParser(description='Count character frequencies in a text file.')
    parser.add_argument('-c', action='store_true', help='distinguish between upper and lower case')
    parser.add_argument('-l', type=str, metavar='letters', help='only print given letters')
    parser.add_argument('-z', action='store_true', help='print zeros for missing letters')
    parser.add_argument('files', type=str, nargs='+', help='input files')

    # Parse arguments
    args = parser.parse_args()

    # 2) Create an empty dictionary
    char_freq_dict = {}

    # This portion iterates through each file in the provided files list and calls the add_frequencies() 
    # function, passing in the 'd' dictionary, the current file, and the boolean value for 'remove_case'. 
    # If the '-c' flag is included in the command line arguments, 'remove_case' should be set to False, 
    # preserving case sensitivity. Otherwise, 'remove_case' should be set to True to remove case sensitivity.
    for file in args.files:
        add_frequencies(char_freq_dict, file, remove_case=not args.c)

    # Argument -l
    if args.l:
        letter_set = set(args.l.lower())

        # 3) Add the frequencies for each file in the argument list to that dictionary
        char_freq_dict = {key: value for key, value in char_freq_dict.items() if key.lower() in letter_set}

        # Sort the char_freq_dict
        char_freq_dict = dict(sorted(char_freq_dict.items()))

    # Argument -z
    if args.z:
        all_letters = set('abcdefghijklmnopqrstuvwxyz')

        # 3) Add the frequencies for each file in the argument list to that dictionary
        char_freq_dict = {key: char_freq_dict.get(key, 0) for key in all_letters}

        # Sort the char_freq_dict
        char_freq_dict = dict(sorted(char_freq_dict.items()))
        
    # 4) Print out the elements of that dictionary in CSV format
    writer = csv.writer(sys.stdout)
    writer.writerows((f"{key}: {value}",) for key, value in char_freq_dict.items())


# Main driver. 
if __name__ == '__main__':
    main()
